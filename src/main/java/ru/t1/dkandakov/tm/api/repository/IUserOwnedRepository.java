package ru.t1.dkandakov.tm.api.repository;

import ru.t1.dkandakov.tm.enumerated.Sort;
import ru.t1.dkandakov.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(String userId);

    boolean existsById(String UserId, String Id);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    List<M> findAll(String userId, Sort sort);

    M findOneById(String useId, String Id);

    M findOneByIndex(String userId, Integer index);

    int getSize(String userId);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

    M add(final String UserId, M model);

    M remove(final String userId, M model);

}






