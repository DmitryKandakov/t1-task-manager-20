package ru.t1.dkandakov.tm.exception.user;

public class EmailEmptyException extends AbstractUserException {

    public EmailEmptyException() {
        super("Error! Email is empty...");
    }

}
